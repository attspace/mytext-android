package attspace.com.mytext;

import android.content.Context;
import android.content.Intent;

import com.clover_studio.spikachatmodule.models.Config;
import com.clover_studio.spikachatmodule.models.User;

import attspace.com.mytext.model.ChatRoom;

/**
 * Created by desmondyong on 04/11/2016.
 */

public class FlowController {

    public static FlowController instant = new FlowController();

    public void goMain(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    public void goChat(Context context, User user, ChatRoom chatRoom){
        Config config = new Config();
        config.apiBaseUrl = "http://54.251.171.27:80/spika/v1/";
        config.socketUrl = "http://54.251.171.27:80/spika";
        MyTextChatActivity.startChatActivityWithConfig(context, user, chatRoom, config);
//        MyTextChatActivity.startChatActivityWithConfig(context, user, config);

    }
}
