package attspace.com.mytext.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by desmondyong on 14/11/2016.
 */

public class ChatRoom extends RealmObject{

    @PrimaryKey
    public String roomId;
    public MyTextUser me;
    public RealmList<MyTextUser> activeUsers;

}
