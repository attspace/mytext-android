package attspace.com.mytext.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by desmondyong on 14/11/2016.
 */
public class MyTextUser extends RealmObject {
    @PrimaryKey
    public String userid;

    public String nickname;

    public String thumbnail;

}
