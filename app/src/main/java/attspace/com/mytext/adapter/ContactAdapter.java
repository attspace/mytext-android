package attspace.com.mytext.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clover_studio.spikachatmodule.models.User;
import com.l4digital.fastscroll.FastScroller;
import com.squareup.picasso.Picasso;

import java.util.List;

import attspace.com.mytext.R;
import attspace.com.mytext.model.ChatRoom;
import attspace.com.mytext.model.Contact;
import attspace.com.mytext.model.MyTextUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by desmondyong on 08/11/2016.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> implements FastScroller.SectionIndexer {
    private List<Contact> contactList;
    private Context mContext;

    public ContactAdapter(Context context, List<Contact> contactList) {
        this.contactList = contactList;
        this.mContext = context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_contact, null);
        ContactViewHolder viewHolder = new ContactViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder customViewHolder, int i) {
        final Contact contact = contactList.get(i);

        //Render image using Picasso library
        if (!TextUtils.isEmpty(contact.thumbnail)) {
            Picasso.with(mContext).load(contact.thumbnail)
                    .error(R.drawable.ic_stickers)
                    .placeholder(R.drawable.ic_stickers)
                    .into(customViewHolder.imageView);
        }else
            customViewHolder.imageView.setImageResource(R.drawable.ic_stickers);

        //Setting text view title
        customViewHolder.TVtitle.setText(contact.title);
        customViewHolder.TVsubtitle.setText(contact.subtitle);

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = generateUserModel("123123", "Test MyTextUser", "", "Room1");
                final ChatRoom chatroom = new ChatRoom();

                final MyTextUser me = new MyTextUser();
                me.userid = "+60107601629";
                me.nickname = "Desmond";
                me.thumbnail = "https://scontent-kul1-1.xx.fbcdn.net/v/t1.0-9/14470559_10155363963168289_9124887402233334161_n.jpg?oh=1a8369af3763a9957c1bc9ac49315821&oe=58D2160E";

                final MyTextUser chatUser = new MyTextUser();
                chatUser.userid = contact.subtitle;
                chatUser.nickname = contact.title;
                chatUser.thumbnail = contact.thumbnail;

                final Realm realm = Realm.getDefaultInstance();
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        chatroom.roomId = me.userid + chatUser.userid;
                        final MyTextUser _me = realm.copyToRealmOrUpdate(me);
                        final MyTextUser _chatUser = realm.copyToRealmOrUpdate(chatUser);
                        final ChatRoom _chatroom = realm.copyToRealmOrUpdate(chatroom);
                        _chatroom.me = _me;
                        _chatroom.activeUsers.add(_chatUser);
                        realm.copyToRealmOrUpdate(_chatroom);
//                        realm.close();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != contactList ? contactList.size() : 0);
    }

    @Override
    public String getSectionText(int position) {
        String first = contactList.get(position).title.substring(0, 1);
        return first;
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail)
        ImageView imageView;

        @BindView(R.id.title)
        TextView TVtitle;

        @BindView(R.id.subtitle)
        TextView TVsubtitle;

        public ContactViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private User generateUserModel(String userID, String name, String avatar, String roomId) {
        User user = new User();

        user.userID = userID;
        user.name = name;
        user.avatarURL = avatar;
        user.roomID = roomId;

        return user;
    }
}
