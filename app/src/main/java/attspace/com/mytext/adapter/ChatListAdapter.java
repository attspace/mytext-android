package attspace.com.mytext.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clover_studio.spikachatmodule.models.User;
import com.l4digital.fastscroll.FastScroller;
import com.squareup.picasso.Picasso;

import java.util.List;

import attspace.com.mytext.FlowController;
import attspace.com.mytext.R;
import attspace.com.mytext.custom.CircularTextView;
import attspace.com.mytext.model.ChatRoom;
import attspace.com.mytext.model.MyTextUser;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by desmondyong on 10/11/2016.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatViewHolder> implements FastScroller.SectionIndexer {
    private List<ChatRoom> chatList;
    private Context mContext;

    public ChatListAdapter(Context context, List<ChatRoom> chatList) {
        this.chatList = chatList;
        this.mContext = context;
    }

    @Override
    public ChatListAdapter.ChatViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_chat, null);
        return new ChatListAdapter.ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatListAdapter.ChatViewHolder customViewHolder, int i) {
        final ChatRoom chatRoom = chatList.get(i);
        final MyTextUser user = chatRoom.activeUsers.first();
        //Render image using Picasso library
        if (!TextUtils.isEmpty(user.thumbnail)) {
            Picasso.with(mContext).load(user.thumbnail)
                    .error(R.drawable.ic_stickers)
                    .placeholder(R.drawable.ic_stickers)
                    .into(customViewHolder.imageView);
        }else{
            customViewHolder.imageView.setImageResource(R.drawable.ic_stickers);

        }

        //Setting text view title
        customViewHolder.TVtitle.setText(chatRoom.activeUsers.first().nickname);
        customViewHolder.TVsubtitle.setText(chatRoom.activeUsers.first().nickname);

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = generateUserModel("123123", "Test MyTextUser", "", chatRoom.roomId);
//                ChatRoom chatRoom = new ChatRoom();
//                chatRoom.roomId = contact.title + contact.subtitle;
                FlowController.instant.goChat(mContext, user, chatRoom);
            }
        });
    }

    private User generateUserModel(String userID, String name, String avatar, String roomId) {
        User user = new User();

        user.userID = userID;
        user.name = name;
        user.avatarURL = avatar;
        user.roomID = roomId;

        return user;
    }

    @Override
    public int getItemCount() {
        return (null != chatList ? chatList.size() : 0);
    }

    @Override
    public String getSectionText(int position) {
        String first = chatList.get(position).activeUsers.first().nickname.substring(0, 1);
        return first;
    }

    class ChatViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_chat_thumbnail)
        ImageView imageView;

        @BindView(R.id.cv_chat_title)
        TextView TVtitle;

        @BindView(R.id.cv_chat_subtitle)
        TextView TVsubtitle;

        @BindView(R.id.cv_chat_bubble)
        CircularTextView TVbuble;

        public ChatViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            TVbuble.setSolidColor("#3F51B5");
        }
    }
}
