package attspace.com.mytext;

import android.app.Application;

import com.karumi.dexter.Dexter;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by desmondyong on 08/11/2016.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("mytext.realm")
//                .encryptionKey(getKey())
                .schemaVersion(3)
                .deleteRealmIfMigrationNeeded()
//                .modules(new MySchemaModule())
//                .migration(new MyMigration())
                .build();
        Realm.setDefaultConfiguration(config);

    }
}
