package attspace.com.mytext.api.retrofit;

import attspace.com.mytext.model.ChatRoom;
import attspace.com.mytext.model.GetListModel;
import attspace.com.mytext.model.GetObjectModel;
import attspace.com.mytext.model.MyTextUser;
import attspace.com.mytext.utils.Const;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by desmondyong on 14/11/2016.
 */

public interface IUserApi {

    @GET(Const.Api.USER_LOGIN)
    Call<GetObjectModel<MyTextUser>> login();

    @GET(Const.Api.GET_USER)
    Call<GetObjectModel<MyTextUser>> getUser();

    @GET(Const.Api.GET_USER_CHATROOM)
    Call<GetListModel<ChatRoom>> getChatRoom();

}