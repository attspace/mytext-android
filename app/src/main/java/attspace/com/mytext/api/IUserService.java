package attspace.com.mytext.api;

import attspace.com.mytext.model.ChatRoom;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by desmondyong on 15/11/2016.
 */

public class IUserService {

    public static IUserService instant = new IUserService();

    public void getChatRoom(RealmChangeListener<RealmResults<ChatRoom>> listener) {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<ChatRoom> chatrooms = realm.where(ChatRoom.class).findAllAsync();
        chatrooms.addChangeListener(listener);
    }
}
