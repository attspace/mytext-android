package attspace.com.mytext;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.l4digital.fastscroll.FastScrollRecyclerView;

import java.util.ArrayList;
import java.util.List;

import attspace.com.mytext.adapter.ChatListAdapter;
import attspace.com.mytext.api.IUserService;
import attspace.com.mytext.model.ChatRoom;
import attspace.com.mytext.model.MyTextUser;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by desmondyong on 10/11/2016.
 */

public class ChatsFragment extends Fragment {

    @BindView(R.id.caf_rv_chat)
    FastScrollRecyclerView recyclerView;

    @BindView(R.id.caf_rv_progress_bar)
    ProgressBar progressBar;

    private static final String TAG = "RecyclerViewExample";
    private List<ChatRoom> chatroomsList;
    private ChatListAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chats, container, false);
        ButterKnife.bind(this, rootView);

        chatroomsList = new ArrayList<ChatRoom>();
        adapter = new ChatListAdapter(this.getActivity(), chatroomsList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        IUserService.instant.getChatRoom(new RealmChangeListener<RealmResults<ChatRoom>>() {
            @Override
            public void onChange(RealmResults<ChatRoom> element) {
                adapter = new ChatListAdapter(ChatsFragment.this.getActivity(), element);
                recyclerView.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);

                Log.d("element", "element.size(): " + element.size());
                int roomcount = 0;
                for (ChatRoom chatroom : element) {
                    Log.d("element", "- "+roomcount + " chatroom id: " + chatroom.roomId);
                    int usercount = 0;
                    for (MyTextUser user : chatroom.activeUsers) {
                        Log.d("element", "---- "+usercount + " user id: " + user.userid + " " + user.nickname);
                        usercount++;
                    }
                    roomcount++;
                }
            }
        });

    }

}
