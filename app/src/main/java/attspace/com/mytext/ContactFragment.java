package attspace.com.mytext;

import android.Manifest;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.l4digital.fastscroll.FastScrollRecyclerView;

import java.util.ArrayList;
import java.util.List;

import attspace.com.mytext.adapter.ContactAdapter;
import attspace.com.mytext.model.Contact;
import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by desmondyong on 04/11/2016.
 */

public class ContactFragment extends Fragment {

    @BindView(R.id.cf_rv_contact)
    FastScrollRecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private static final String TAG = "RecyclerViewExample";
    private List<Contact> feedsList;
    private ContactAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        ButterKnife.bind(this, rootView);

        feedsList = new ArrayList<Contact>();
        adapter = new ContactAdapter(this.getActivity(), feedsList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dexter.checkPermissions(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                Cursor phones = getActivity()
                        .getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                null,
                                null,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

                while (phones.moveToNext()) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String photoUrl = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));

                    Contact contact = new Contact();
                    contact.title = name;
                    contact.subtitle = phoneNumber;
                    contact.thumbnail = photoUrl;
                    feedsList.add(contact);
                }
                phones.close();

                adapter = new ContactAdapter(ContactFragment.this.getActivity(), feedsList);
                recyclerView.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CALENDAR);

    }

}

