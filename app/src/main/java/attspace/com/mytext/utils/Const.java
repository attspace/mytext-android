package attspace.com.mytext.utils;

/**
 * Created by desmondyong on 14/11/2016.
 */

public class Const {
    public static final class Api{
        public static final String BASE_URL = "http://ossdemo.spika.chat/spika/v1/"; //new server url

        public static final String USER_LOGIN = "user/login";
        public static final String GET_USER = "user/profile";
        public static final String GET_USER_CHATROOM = "user/chatroom";

    }
}
