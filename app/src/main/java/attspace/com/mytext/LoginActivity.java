package attspace.com.mytext;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by desmondyong on 04/11/2016.
 */

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.li_tv_phonenumber)
    EditText etPhoneNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.li_bt_next)
    protected void next(){
        FlowController.instant.goMain(this);
    }
}
